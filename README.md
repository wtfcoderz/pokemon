## Ex d'API

- Carte : https://api.pokemontcg.io/v1/cards/sm9-4
- Cartes d'un set : https://api.pokemontcg.io/v1/cards/cards?setCode=sm9
- Set : https://api.pokemontcg.io/v1/sets/sm9

- Doc : https://docs.pokemontcg.io/

## API

### Auth

- POST /auth/register => Create account
- POST /auth/login => Return token
- GET /auth/login => Verify login (need jwt token)

### Deck

- POST /deck => Create new deck
- PUT /deck/<deckid> => Modify deck
- GET /deck/<deckid>

### Game

- POST /game => Return game ID
- GET /game/<gameid>
- POST /game/<gameid>/join => A faire par le second joueur
- POST /game/<gameid>/deck/choose => A faire par les 2 joueurs

### Actions

- POST /game/<gameid>/choosemainpokemon => A faire par les 2 joueurs


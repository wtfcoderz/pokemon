// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/wtfcoderz/pokemon/models"
	"gitlab.com/wtfcoderz/pokemon/restapi/operations"
	"gitlab.com/wtfcoderz/pokemon/restapi/operations/health"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-redis/redis"
)

//go:generate swagger generate server --target ../../pokemon --name APIPokemon --spec ../swagger.yml --principal interface{} --exclude-main

var ctx = context.Background()
var rdb *redis.Client
var jwtSecret = []byte("my_secret_key")

type User struct {
	Username     string `json:"username"`
	PasswordHash string `json:"password_hash"`
}

func initRedis() {
	rdb = redis.NewClient(&redis.Options{
		Addr:     "localhost:6379", // use default Addr
		Password: "",               // no password set
		DB:       0,                // use default DB
	})
	pong, err := rdb.Ping(ctx).Result()
	fmt.Println(pong, err)
}

func userExists(u string) bool {
	_, err := rdb.Get(ctx, "user:"+u).Result()
	if err == redis.Nil {
		return false
	}
	return true
}

func insertUser(u string, p string) {
	hash, err := bcrypt.GenerateFromPassword([]byte(p), bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}

	user := User{}
	user.Username = u
	user.PasswordHash = string(hash)
	b, err := json.Marshal(user)
	if err != nil {
		log.Println(err)
	}

	err = rdb.Set(ctx, "user:"+u, string(b), 0).Err()
	if err != nil {
		log.Println(err)
	}
}

func jwtGenerate(username string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["username"] = username
	claims["exp"] = time.Now().Add(time.Minute * 300).Unix()

	tokenString, err := token.SignedString(jwtSecret)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func jwtValidateHeader(bearerHeader string) (interface{}, error) {
	bearerToken := strings.Split(bearerHeader, " ")[1]
	claims := jwt.MapClaims{}
	token, err := jwt.ParseWithClaims(bearerToken, claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("error decoding token")
		}
		return jwtSecret, nil
	})
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	if token.Valid {
		return claims["username"].(string), nil
	}
	err = errors.New(500, "invalid token")
	return nil, err
}

func configureFlags(api *operations.APIPokemonAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.APIPokemonAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	initRedis()
	api.BearerAuth = jwtValidateHeader

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.UseSwaggerUI()
	// To continue using redoc as your UI, uncomment the following line
	// api.UseRedoc()

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	api.HealthGetHealthHandler = health.GetHealthHandlerFunc(func(params health.GetHealthParams) middleware.Responder {
		return health.NewGetHealthOK()
		//return middleware.NotImplemented("operation health.GetHealth has not yet been implemented")
	})
	api.PostAuthRegisterHandler = operations.PostAuthRegisterHandlerFunc(func(params operations.PostAuthRegisterParams) middleware.Responder {
		fmt.Println(*params.User.Username)
		if userExists(*params.User.Username) {
			return operations.NewPostAuthRegisterConflict()
		}
		insertUser(*params.User.Username, *params.User.Password)
		return operations.NewPostAuthRegisterCreated()
		//return middleware.NotImplemented("operation operations.PostAuthRegister has not yet been implemented")
	})

	api.GetAuthLoginHandler = operations.GetAuthLoginHandlerFunc(func(params operations.GetAuthLoginParams, principal interface{}) middleware.Responder {
		username, err := jwtValidateHeader(params.HTTPRequest.Header.Get("Authorization"))
		if err != nil {
			response := &models.APIResponse{
				Success: false,
				Message: "Error in parsing token",
			}
			return operations.NewGetAuthLoginInternalServerError().WithPayload(response)
		}

		response := &models.APIResponse{
			Success: true,
			Message: "User: " + username.(string),
		}
		return operations.NewGetAuthLoginOK().WithPayload(response)
	})

	api.PostAuthLoginHandler = operations.PostAuthLoginHandlerFunc(func(params operations.PostAuthLoginParams) middleware.Responder {
		// TODO Verify login here see https://gist.githubusercontent.com/shashankvivek/8a3b04615e09a7c5d88764ce59b6a5e4/raw/efb611bfab7302a38d9fe09901c4378bf2e1ae69/login.go
		/*email := params.Login.Email
		userInfo, err := dao.FetchUserDetails(impl.dbClient, *email)
		if err != nil {
			fmt.Println(err.Error())
			return user.NewLoginInternalServerError().WithPayload("Error fetching user details")
		}
		err = bcrypt.CompareHashAndPassword([]byte(*userInfo.Password), []byte(*params.Login.Password))
		if err != nil {
			fmt.Println(err)
			return user.NewRegisterNotFound()
		}*/

		token, err := jwtGenerate(*params.User.Username)
		if err != nil {
			return operations.NewPostAuthLoginOK()
		}
		return operations.NewPostAuthLoginOK().WithPayload(&models.LoginSuccess{Success: true, Token: token})
	})

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
